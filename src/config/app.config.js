import authInterceptor from './auth.interceptor';
import appViewTemplate from '../layout/app-view.html';
import { store } from '../store';

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider, $ngReduxProvider) {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('app', {
      abstract: true,
      template: appViewTemplate,
      resolve: {
        auth: function(User) {
          return User.verifyAuth();
        },
      },
    });

  $urlRouterProvider.otherwise('/');
  $ngReduxProvider.provideStore(store);
}

export default AppConfig;
