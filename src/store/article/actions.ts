import {
  ArticleActionTypes,
  FETCH_ARTICLE_SUCCEED,
  FETCH_COMMENTS_SUCCEED,
  IArticle, IAuthor,
  IComment, RATE_ARTICLE,
  REMOVE_ARTICLE_SUCCEED,
  TOGGLE_FAVORITE_STATE,
  UPDATE_AUTHOR_SUCCEED,
} from './types';

export function fetchArticleSucceed(article: IArticle): ArticleActionTypes {
  return {
    payload: article,
    type: FETCH_ARTICLE_SUCCEED,
  };
}

export function fetchCommentsSucceed(slug: string, comments: IComment[]): ArticleActionTypes {
  return {
    payload: {
      comments,
      slug,
    },
    type: FETCH_COMMENTS_SUCCEED,
  };
}

export function updateAuthorSucceed(author: IAuthor): ArticleActionTypes {
  return {
    payload: author,
    type: UPDATE_AUTHOR_SUCCEED,
  };
}

export function toggleFavoriteState(slug: string, state: boolean): ArticleActionTypes {
  return {
    payload: {
      meta: {
        slug,
        state,
      },
    },
    type: TOGGLE_FAVORITE_STATE,
  };
}

export function rateArticle(slug: string, author: IAuthor, value: number): ArticleActionTypes {
  return {
    payload: {
      author,
      slug,
      value,
    },
    type: RATE_ARTICLE,
  };
}

export function removeArticleSucceed(slug: string): ArticleActionTypes {
  return {
    payload: {
      slug,
    },
    type: REMOVE_ARTICLE_SUCCEED,
  };
}
