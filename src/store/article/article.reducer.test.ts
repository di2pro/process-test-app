import {
  fetchArticleSucceed,
  fetchCommentsSucceed,
  rateArticle,
  removeArticleSucceed,
  toggleFavoriteState,
  updateAuthorSucceed,
} from './actions';
import articleReducer from './article.reducer';

describe('articleReducer', () => {
  it('should store the articles by slugs', () => {
    const article = {
      author: {
        bio: 'bio',
        following: false,
        image: 'image',
        username: 'username',
      },
      body: 'body',
      createdAt: 'createdAt',
      description: 'description',
      favorited: false,
      favoritesCount: 0,
      slug: 'slug',
      tagList: [],
      title: 'title',
      updatedAt: 'updatedAt',
    };
    const action = fetchArticleSucceed(article);
    const { articles } = articleReducer(undefined, action);
    expect(articles).toEqual({
      [article.slug]: article,
    });
  });

  it('should store the comments by article slug', () => {
    const comments = [{
      author: {
        bio: 'bio',
        following: false,
        image: 'image',
        username: 'username',
      },
      body: 'body',
      createdAt: 'createdAt',
      id: 1,
    }];
    const action = fetchCommentsSucceed('slug', comments);
    const state = articleReducer(undefined, action);
    expect(state.comments).toEqual({
      slug: comments,
    });
  });

  it('should update author in the articles', () => {
    const updatedAuthor = {
      bio: 'bio',
      following: true,
      image: 'image',
      username: 'username',
    };
    const action = updateAuthorSucceed(updatedAuthor);
    const { articles } = articleReducer({
      articles: {
        slug: {
          author: {
            bio: 'bio',
            following: false,
            image: 'image',
            username: 'username',
          },
          body: 'body',
          createdAt: 'createdAt',
          description: 'description',
          favorited: false,
          favoritesCount: 0,
          slug: 'slug',
          tagList: [],
          title: 'title',
          updatedAt: 'updatedAt',
        },
      },
      comments: {},
      ratings: {},
    }, action);
    expect(articles).toEqual({
      slug: {
        author: {
          bio: 'bio',
          following: true,
          image: 'image',
          username: 'username',
        },
        body: 'body',
        createdAt: 'createdAt',
        description: 'description',
        favorited: false,
        favoritesCount: 0,
        slug: 'slug',
        tagList: [],
        title: 'title',
        updatedAt: 'updatedAt',
      },
    });
  });

  it('should update favorites in the article', () => {
    const action = toggleFavoriteState('slug', true);
    const { articles } = articleReducer({
      articles: {
        slug: {
          author: {
            bio: 'bio',
            following: false,
            image: 'image',
            username: 'username',
          },
          body: 'body',
          createdAt: 'createdAt',
          description: 'description',
          favorited: false,
          favoritesCount: 0,
          slug: 'slug',
          tagList: [],
          title: 'title',
          updatedAt: 'updatedAt',
        },
      },
      comments: {},
      ratings: {},
    }, action);
    expect(articles).toEqual({
      slug: {
        author: {
          bio: 'bio',
          following: false,
          image: 'image',
          username: 'username',
        },
        body: 'body',
        createdAt: 'createdAt',
        description: 'description',
        favorited: true,
        favoritesCount: 1,
        slug: 'slug',
        tagList: [],
        title: 'title',
        updatedAt: 'updatedAt',
      },
    });
  });

  it('should store article rating', () => {
    const author = {
      bio: 'bio',
      following: false,
      image: 'image',
      username: 'username',
    };
    const action = rateArticle('slug', author, 3);
    const { ratings } = articleReducer(undefined, action);
    expect(ratings).toEqual({
      slug: [{
        author,
        slug: 'slug',
        value: 3,
      }],
    });
  });

  it('should remove article', () => {
    const action = removeArticleSucceed('slug');
    const { articles } = articleReducer({
      articles: {
        slug: {
          author: {
            bio: 'bio',
            following: false,
            image: 'image',
            username: 'username',
          },
          body: 'body',
          createdAt: 'createdAt',
          description: 'description',
          favorited: false,
          favoritesCount: 0,
          slug: 'slug',
          tagList: [],
          title: 'title',
          updatedAt: 'updatedAt',
        },
      },
      comments: {},
      ratings: {},
    }, action);
    expect(articles).toEqual({});
  });
});
