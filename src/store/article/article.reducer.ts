import { createReducer } from 'redux-starter-kit';
import {
  FETCH_ARTICLE_SUCCEED,
  FETCH_COMMENTS_SUCCEED,
  IArticleReducerState,
  RATE_ARTICLE,
  REMOVE_ARTICLE_SUCCEED,
  TOGGLE_FAVORITE_STATE,
  UPDATE_AUTHOR_SUCCEED,
} from './types';

const initialState: IArticleReducerState = {
  articles: {},
  comments: {},
  ratings: {},
};

export default createReducer(initialState, {
  [FETCH_ARTICLE_SUCCEED]: (state, { payload }) => ({
    ...state,
    articles: {
      ...state.articles,
      [payload.slug]: payload,
    },
  }),
  [FETCH_COMMENTS_SUCCEED]: (state, { payload }) => ({
    ...state,
    comments: {
      ...state.comments,
      [payload.slug]: payload.comments,
    },
  }),
  [UPDATE_AUTHOR_SUCCEED]: (state, { payload }) => {
    const slugs = Object.keys(state.articles);
    const updatedArticles = slugs.reduce((map, slug) => {
      if (state.articles[slug].author.username === payload.username) {
        return {
          ...map,
          [slug]: {
            ...state.articles[slug],
            author: payload,
          },
        };
      }

      return {
        ...map,
        [slug]: state.articles[slug],
      };
    }, {});

    return {
      ...state,
      articles: updatedArticles,
    };
  },
  [TOGGLE_FAVORITE_STATE]: (state, { payload }) => {
    const {slug, state: articleState} = payload.meta;

    return {
      ...state,
      articles: {
        ...state.articles,
        [slug]: {
          ...state.articles[slug],
          favorited: articleState,
          favoritesCount: articleState
            ? state.articles[slug].favoritesCount + 1
            : state.articles[slug].favoritesCount - 1,
        },
      },
    };
  },
  [RATE_ARTICLE]: (state, { payload }) => {
    const { slug, author, value } = payload;
    let updatedRatings;

    if (!state.ratings[slug]) {
      updatedRatings = {
        ...state.ratings,
        [slug]: [{ slug, author, value }],
      };
    } else {
      updatedRatings = {
        ...state.ratings,
        [slug]: [
          ...state.ratings[slug],
          { slug, author, value },
        ],
      };
    }

    return {
      ...state,
      ratings: updatedRatings,
    };
  },
  [REMOVE_ARTICLE_SUCCEED]: (state, { payload }) => {
    const { [payload.slug]: removedArticle, ...updatedArticles } = state.articles;

    return {
      ...state,
      articles: updatedArticles,
    };
  },
});
