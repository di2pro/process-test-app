export interface IDictionary<TValue> {
  [id: string]: TValue;
}

export interface IAuthor {
  bio: string;
  following: boolean;
  image: string;
  username: string;
}

export interface IArticle {
  author: IAuthor;
  body: string;
  description: string;
  favorited: boolean;
  favoritesCount: number;
  slug: string;
  tagList: string[];
  title: string;
  createdAt: string;
  updatedAt: string;
}

export interface IComment {
  id: number;
  body: string;
  createdAt: string;
  author: IAuthor;
}

export interface IRating {
  slug: string;
  author: IAuthor;
  value: number;
}

export interface IArticleReducerState {
  articles: IDictionary<IArticle>;
  comments: IDictionary<IComment[]>;
  ratings: IDictionary<IRating[]>;
}

export const FETCH_ARTICLE_SUCCEED = 'FETCH_ARTICLE_SUCCEED';
export const FETCH_COMMENTS_SUCCEED = 'FETCH_COMMENTS_SUCCEED';
export const UPDATE_AUTHOR_SUCCEED = 'UPDATE_AUTHOR_SUCCEED';
export const TOGGLE_FAVORITE_STATE = 'TOGGLE_FAVORITE_STATE';
export const RATE_ARTICLE = 'RATE_ARTICLE';
export const REMOVE_ARTICLE_SUCCEED = 'REMOVE_ARTICLE_SUCCEED';

interface IFetchArticleSucceedAction {
  type: typeof FETCH_ARTICLE_SUCCEED;
  payload: IArticle;
}

interface IFetchCommentsSucceedAction {
  type: typeof FETCH_COMMENTS_SUCCEED;
  payload: {
    slug: string,
    comments: IComment[],
  };
}

interface IUpdateAuthorSucceedAction {
  type: typeof UPDATE_AUTHOR_SUCCEED;
  payload: IAuthor;
}

interface IToggleFavoriteStateAction {
  type: typeof TOGGLE_FAVORITE_STATE;
  payload: {
    meta: {
      slug: string,
      state: boolean,
    },
  };
}

interface IRateArticleAction {
  type: typeof RATE_ARTICLE;
  payload: IRating;
}

interface IRemoveArticleAction {
  type: typeof REMOVE_ARTICLE_SUCCEED;
  payload: {
    slug: string,
  };
}

export type ArticleActionTypes = IFetchArticleSucceedAction |
  IFetchCommentsSucceedAction |
  IUpdateAuthorSucceedAction |
  IToggleFavoriteStateAction |
  IRateArticleAction |
  IRemoveArticleAction;
