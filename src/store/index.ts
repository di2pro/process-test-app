import createConnect from 'redux-connect-standalone';
import { configureStore } from 'redux-starter-kit';

import articleReducer from './article/article.reducer';

export const store = configureStore({
  reducer: articleReducer,
});

export const connect = createConnect(store);
