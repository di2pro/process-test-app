import appFooterTemplate from './footer.html';

class AppFooterCtrl {
  constructor(AppConstants) {
    'ngInject';
    this.appName = AppConstants.appName;

    // Get today's date to generate the year
    this.date = new Date();
  }
}

const AppFooter = {
  controller: AppFooterCtrl,
  template: appFooterTemplate,
};

export default AppFooter;
