import * as React from 'react';

import { IArticle } from '../store/article/types';

type PropTypes = {
  article: IArticle;
  canModify: boolean;
  goTo(route: string, options?: object): void;
  addFavorite(): Promise<void>;
  follow(): Promise<void>;
  remove(): void;
};

type State = {
  isDeleting: boolean;
  isFollowing: boolean;
  isAddingToFavorites: boolean;
};

export default class ArticleActions extends React.PureComponent<PropTypes, State> {
  constructor(props) {
    super(props);

    this.state = {
      isAddingToFavorites: false,
      isDeleting: false,
      isFollowing: false,
    };

    this.goToProfile = this.goToProfile.bind(this);
    this.goToEditor = this.goToEditor.bind(this);
    this.deleteArticle = this.deleteArticle.bind(this);
    this.addToFavorites = this.addToFavorites.bind(this);
    this.followUser = this.followUser.bind(this);
  }

  public deleteArticle(): void {
    const { remove } = this.props;

    this.setState({ isDeleting: true });
    remove();
  }

  public async followUser(): Promise<void> {
    const { follow } = this.props;

    this.setState({ isFollowing: true });
    await follow();
    this.setState({isFollowing: false });
  }

  public async addToFavorites(): Promise<void> {
    const { addFavorite } = this.props;

    this.setState({ isAddingToFavorites: true });
    await addFavorite();
    this.setState({isAddingToFavorites: false });
  }

  public goToProfile(event: React.MouseEvent): void {
    event.preventDefault();

    const { goTo, article } = this.props;

    goTo('app.profile.main', {
      username: article.author.username,
    });
  }

  public goToEditor(event: React.MouseEvent): void {
    event.preventDefault();

    const { goTo, article } = this.props;

    goTo('app.editor', {
      slug: article.slug,
    });
  }

  get userActions() {
    const { isAddingToFavorites, isFollowing, isDeleting } = this.state;
    const { canModify, article } = this.props;

    if (canModify) {
      return (
        <span>
          <a className='btn btn-sm btn-outline-secondary' href='#' onClick={this.goToEditor}>
            <i className='ion-edit'/> Edit Article
          </a>
          &nbsp;
          <button
            className={`btn btn-sm btn-outline-danger ${isDeleting ? 'disabled' : ''}`}
            onClick={this.deleteArticle}
          >
            <i className='ion-trash-a'/> Delete Article
          </button>
        </span>
      );
    }

    return (
      <span>
        <button
          className={`btn btn-sm action-btn ${isFollowing ? 'disabled' : ''}`}
          onClick={this.followUser}
        >
          <i className='ion-plus-round'/>&nbsp;
          {article.author.following ? 'Unfollow' : 'Follow'} {article.author.username}
        </button>&nbsp;
        <button className={`btn btn-sm ${isAddingToFavorites ? 'disabled' : ''}`} onClick={this.addToFavorites}>
          <i className='ion-heart'/>&nbsp;
          {article.favorited ? 'Unfavorite' : 'Favorite'} Article&nbsp;
          <span className='counter'>{article.favoritesCount}</span>
        </button>
      </span>
    );
  }

  public render() {
    const { article } = this.props;

    return (
      <React.Fragment>
        <div className='article-meta'>
          <a href='#' onClick={this.goToProfile}>
            <img src={article.author.image} alt={article.author.username} />
          </a>

          <div className='info'>
            <a className='author' href='#' onClick={this.goToProfile}>
              {article.author.username}
            </a>
            <span className='date'>{new Date(article.createdAt).toLocaleString()}</span>
        </div>
        {this.userActions}
      </div >
      </React.Fragment >
    );
  }
}
