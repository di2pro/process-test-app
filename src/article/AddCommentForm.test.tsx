import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';

import AddCommentForm from './AddCommentForm';

describe('<AddCommentForm>', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<AddCommentForm />);
  });

  it('should render without errors', () => {
    expect(wrapper).toExist();
  });
});
