import angular from 'angular';
import { react2angular } from 'react2angular';

import ArticleConfig from './article.config';
import Article from './Article';

const articleModule = angular.module('app.article', []);

articleModule.config(ArticleConfig);
articleModule.component(
  'article',
  react2angular(
    Article,
    ['article'],
    ['$rootScope', '$state', '$stateParams', 'Articles', 'User', 'Comments', 'Profile']
  ));

export default articleModule;
