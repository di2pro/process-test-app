import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';

import ArticleActions from './ArticleActions';

describe('<ArticleActions>', () => {
  let wrapper;
  const defaultProps = {
    addFavorite: jest.fn(),
    article: {
      author: {
        bio: 'bio',
        following: false,
        image: 'image',
        username: 'username',
      },
      body: 'body',
      createdAt: 'createdAt',
      description: 'description',
      favorited: false,
      favoritesCount: 0,
      slug: 'slug',
      tagList: [],
      title: 'title',
      updatedAt: 'updatedAt',
    },
    canModify: false,
    follow: jest.fn(),
    goTo: jest.fn(),
    remove: jest.fn(),
  };

  beforeEach(() => {
    wrapper = shallow(<ArticleActions {...defaultProps} />);
  });

  it('should render without errors', () => {
    expect(wrapper).toExist();
  });
});
