import { shallow } from 'enzyme';
import 'jest-enzyme';
import * as React from 'react';

import Comment from './Comment';

describe('<Comment>', () => {
  let wrapper;
  const defaultProps = {
    author: {
      bio: 'bio',
      following: false,
      image: '',
      username: 'usermane',
    },
    body: 'body',
    canModify: false,
    createdAt: 'createdAt',
  };

  beforeEach(() => {
    wrapper = shallow(<Comment {...defaultProps} />);
  });

  it('should not show remove button with false permission', () => {
    expect(wrapper).not.toContainMatchingElement('.mod-options');
  });

  it('should render remove button with correct permissions', () => {
    wrapper.setProps({ canModify: true });
    expect(wrapper).toContainMatchingElement('.mod-options');
  });
});
