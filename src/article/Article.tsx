import { IRootScopeService } from 'angular';
import { StateParams, StateService } from 'angular-ui-router';
import marked from 'marked';
import * as React from 'react';
import StarRatings from 'react-star-ratings';

import { connect } from '../store';
import * as articleActions from '../store/article/actions';
import {IArticle, IAuthor, IComment, IDictionary, IRating} from '../store/article/types';
import { IArticleService, ICommentService, IProfileService, IUserService } from '../utils/types';
import AddCommentForm from './AddCommentForm';
import ArticleActions from './ArticleActions';
import Comment from './Comment';

type PropTypes = {
  articles: IDictionary<IArticle>;
  comments: IDictionary<IComment[]>;
  ratings: IDictionary<IRating[]>;
  $rootScope: IRootScopeService;
  Articles: IArticleService;
  User: IUserService;
  Comments: ICommentService;
  Profile: IProfileService;
  $stateParams: StateParams;
  $state: StateService;
  rateArticle(slug: string, author: IAuthor, rating: number)
};

type State = {
  isDataFetched: boolean,
};

export class Article extends React.PureComponent<PropTypes, State> {
  constructor(props) {
    super(props);

    this.state = {
      isDataFetched: false,
    };

    this.goTo = this.goTo.bind(this);
    this.goToSignIn = this.goToSignIn.bind(this);
    this.goToSignUp = this.goToSignUp.bind(this);
    this.removeArticle = this.removeArticle.bind(this);
    this.toggleFavorite = this.toggleFavorite.bind(this);
    this.toggleFollowing = this.toggleFollowing.bind(this);
    this.rateArticle = this.rateArticle.bind(this);
  }

  public async componentDidMount(): Promise<void> {
    const { Articles, $stateParams, Comments } = this.props;

    if (!$stateParams.slug) {
      this.goTo('app.home');
    } else {
      try {
        await Articles.get($stateParams.slug);
        await Comments.getAll($stateParams.slug);
        this.setState({ isDataFetched: true });
      } catch (error) {
        this.goTo('app.home');
      }
    }
  }

  public goTo(route: string, options?: object): void {
    const { $state } = this.props;

    $state.go(route, options);
  }

  public goToSignIn(event: React.MouseEvent): void {
    event.preventDefault();

    this.goTo('app.login');
  }

  public goToSignUp(event: React.MouseEvent): void {
    event.preventDefault();

    this.goTo('app.register');
  }

  public toggleFollowing() {
    const { Profile } = this.props;
    const article = this.article;

    if (article.author.following) {
      return Profile.unfollow(article.author.username);
    }

    return Profile.follow(article.author.username);
  }

  public toggleFavorite() {
    const { Articles } = this.props;
    const article = this.article;

    if (article.favorited) {
      return Articles.unfavorite(article.slug);
    }

    return Articles.favorite(article.slug);
  }

  public removeArticle() {
    const { Articles, $state } = this.props;
    const article = this.article;

    Articles.destroy(article.slug);
    $state.go('app.home');
  }

  public rateArticle(value: number) {
    const { rateArticle, User } = this.props;
    const article = this.article;
    const currentUser = User.current;

    rateArticle(article.slug, {
      bio: currentUser.bio,
      following: currentUser.following,
      image: currentUser.image,
      username: currentUser.username,
    }, value);
  }

  get article(): IArticle {
    const { articles, $stateParams } = this.props;

    return articles[$stateParams.slug];
  }

  get tags() {
    const article = this.article;

    return article.tagList.map((tag, index) => (
      <li key={index} className='tag-default tag-pill tag-outline'>
        {tag}
      </li>
    ));
  }

  get commentForm() {
    const { User } = this.props;

    if (!User.current) {
      return (
        <div show-authed='false'>
          <a href='#' onClick={this.goToSignIn}>Sign in</a> or&nbsp;
          <a href='#' onClick={this.goToSignUp}>sign up</a> to add comments on this
          article.
        </div>
      );
    }

    return (<AddCommentForm />);
  }

  get comments() {
    const { comments, $stateParams } = this.props;
    const canModifyComments = this.canModifyPermission;

    return comments[$stateParams.slug].map((comment) => (
      <Comment key={comment.id} canModify={canModifyComments} {...comment} />
    ));
  }

  get articleRating() {
    const { ratings, $stateParams } = this.props;

    if (!ratings[$stateParams.slug]) {
      return 0;
    }

    const ratingsForArticle = ratings[$stateParams.slug];
    const totalRating = ratingsForArticle.reduce((sum, rating) => {
      sum = sum + rating.value;

      return sum;
    }, 0);

    return Math.ceil(totalRating / ratingsForArticle.length);
  }

  get canModifyPermission() {
    const { User } = this.props;
    const article = this.article;

    return User.current && User.current.username === article.author.username;
  }

  public render() {
    const { isDataFetched } = this.state;

    if (!isDataFetched) {
      return null; // TODO: add a spinner
    }

    const article = this.article;
    const canModifyArticle = this.canModifyPermission;

    return (
      <div className='article-page'>
        <div className='banner'>
          <div className='container'>
            <h1>{article.title}</h1>
            <div className='article-meta'>
              <ArticleActions
                article={article}
                goTo={this.goTo}
                canModify={canModifyArticle}
                follow={this.toggleFollowing}
                addFavorite={this.toggleFavorite}
                remove={this.removeArticle}
              />
            </div>
          </div>
        </div>

        <div className='container page'>
          <div className='row article-content'>
            <div className='col-xs-12'>
              <div dangerouslySetInnerHTML={{__html: marked(article.body) }}/>

              <ul className='tag-list'>
                {this.tags}
              </ul>

            </div>
          </div>

          <hr/>
          <StarRatings
            rating={this.articleRating}
            starRatedColor='blue'
            changeRating={this.rateArticle}
            numberOfStars={5}
            name='rating'
            starDimension='24px'
          />
          <div className='article-actions'>
            <ArticleActions
              article={article}
              goTo={this.goTo}
              canModify={canModifyArticle}
              follow={this.toggleFollowing}
              addFavorite={this.toggleFavorite}
              remove={this.removeArticle}
            />
          </div>

          <div className='row'>
            <div className='col-xs-12 col-md-8 offset-md-2'>
              {this.commentForm}
              {this.comments}
            </div>
          </div>

        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ articles, comments, ratings }) => ({
  articles,
  comments,
  ratings,
});

const mapActionsToProps = (dispatch) => ({
  rateArticle: (slug, author, rating) => dispatch(articleActions.rateArticle(slug, author, rating)),
});

export default connect(mapStateToProps, mapActionsToProps)(Article);
