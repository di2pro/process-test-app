import * as React from 'react';

import { IAuthor } from '../store/article/types';

type PropTypes = {
 body: string;
 author: IAuthor;
 createdAt: string;
 canModify: boolean;
};

export default class Comment extends React.PureComponent<PropTypes> {
  public removeComment() {
    // TODO: implement delete comment logic
  }

  get removeButton() {
    const { canModify } = this.props;

    if (!canModify) {
      return null;
    }

    return (
      <span className='mod-options'>
        <i className='ion-trash-a' onClick={this.removeComment} />
      </span>
    );
  }

  public render() {
    const { body, author, createdAt } = this.props;

    return (
      <div className='card'>
        <div className='card-block'>
          <p className='card-text'>{body}</p>
        </div>
        <div className='card-footer'>
          <a className='comment-author' ui-sref='app.profile.main({ username: $ctrl.data.author.username })'>
            <img src={author.image} className='comment-author-img' alt={author.username}/>
          </a>
          &nbsp;
          <a className='comment-author' ui-sref='app.profile.main({ username: $ctrl.data.author.username })'>
            {author.username}
          </a>
          <span className='date-posted'>
            {new Date(createdAt).toLocaleString()}
          </span>
          {this.removeButton}
        </div>
      </div>

    );
  }
}
