function ArticleConfig($stateProvider) {
  'ngInject';

  $stateProvider
    .state('app.article', {
      url: '/article/:slug',
      component: 'article',
      title: 'Article',
    });

}

export default ArticleConfig;
