import { fetchArticleSucceed, toggleFavoriteState, removeArticleSucceed } from '../store/article/actions';

export default class Articles {
  constructor(AppConstants, $http, $q, $ngRedux) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$q = $q;
    this._$ngRedux = $ngRedux;
  }

  /*
    Config object spec:

    {
      type: String [REQUIRED] - Accepts "all", "feed"
      filters: Object that serves as a key => value of URL params (i.e. {author:"ericsimons"} )
    }
  */
  query(config) {
    // Create the $http object for this request
    const request = {
      url: this._AppConstants.api + '/articles' + ((config.type === 'feed') ? '/feed' : ''),
      method: 'GET',
      params: config.filters ? config.filters : null,
    };
    return this._$http(request)
      .then((res) => res.data);
  }

  get(slug) {
    const deferred = this._$q.defer();

    if (!slug.replace(' ', '')) {
      deferred.reject('Article slug is empty');
      return deferred.promise;
    }

    this._$http({
      url: this._AppConstants.api + '/articles/' + slug,
      method: 'GET',
    })
      .then(
        (res) => {
          this._$ngRedux.dispatch(fetchArticleSucceed(res.data.article));
          return deferred.resolve(res.data.article);
        },
        (err) => deferred.reject(err),
      );

    return deferred.promise;
  }

  destroy(slug) {
    return this._$http({
      url: this._AppConstants.api + '/articles/' + slug,
      method: 'DELETE',
    })
      .then(() => {
        this._$ngRedux.dispatch(removeArticleSucceed(slug));
      });
  }

  save(article) {
    const request = {};

    if (article.slug) {
      request.url = `${this._AppConstants.api}/articles/${article.slug}`;
      request.method = 'PUT';
      delete article.slug;

    } else {
      request.url = `${this._AppConstants.api}/articles`;
      request.method = 'POST';
    }

    request.data = { article: article };

    return this._$http(request)
      .then((res) => res.data.article);
  }

  favorite(slug) {
    return this._$http({
      url: this._AppConstants.api + '/articles/' + slug + '/favorite',
      method: 'POST',
    })
      .then(() => {
        this._$ngRedux.dispatch(toggleFavoriteState(slug, true));
      });
  }

  unfavorite(slug) {
    return this._$http({
      url: this._AppConstants.api + '/articles/' + slug + '/favorite',
      method: 'DELETE',
    })
      .then(() => {
        this._$ngRedux.dispatch(toggleFavoriteState(slug, false));
      });
  }

}
