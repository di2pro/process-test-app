import { updateAuthorSucceed } from '../store/article/actions';

export default class Profile {
  constructor(AppConstants, $http, $ngRedux) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$ngRedux = $ngRedux;

  }

  get(username) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/' + username,
      method: 'GET',
    })
      .then((res) => res.data.profile);
  }

  follow(username) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/' + username + '/follow',
      method: 'POST',
    })
      .then((res) => {
        this._$ngRedux.dispatch(updateAuthorSucceed(res.data.profile));
        return res.data;
      });
  }

  unfollow(username) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/' + username + '/follow',
      method: 'DELETE',
    })
      .then((res) => {
        this._$ngRedux.dispatch(updateAuthorSucceed(res.data.profile));
        return res.data;
      });
  }

}
