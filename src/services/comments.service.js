import { fetchCommentsSucceed } from '../store/article/actions';

export default class Comments {
  constructor(AppConstants, $http, $ngRedux) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$ngRedux = $ngRedux;
  }

  // Add a comment to an article
  add(slug, payload) {
    return this._$http({
      url: `${this._AppConstants.api}/articles/${slug}/comments`,
      method: 'POST',
      data: { comment: { body: payload } },
    })
      .then((res) => res.data.comment);

  }

  getAll(slug) {
    return this._$http({
      url: `${this._AppConstants.api}/articles/${slug}/comments`,
      method: 'GET',
    })
      .then((res) => {
        this._$ngRedux.dispatch(fetchCommentsSucceed(slug, res.data.comments));
        return res.data.comments;
      });

  }

  destroy(commentId, articleSlug) {
    return this._$http({
      url: `${this._AppConstants.api}/articles/${articleSlug}/comments/${commentId}`,
      method: 'DELETE',
    });
  }

}
