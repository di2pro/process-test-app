import angular from 'angular';
import 'angular-ui-router';
import 'ng-redux';

// Import our app config files
import constants from './config/app.constants';
import appConfig from './config/app.config';
import appRun from './config/app.run';

// Import our app functionality
import './layout';
import './components';
import './home';
import './profile';
import './article';
import './services';
import './auth';
import './settings';
import './editor';

// Create and bootstrap application
const requires = [
  'ngRedux',
  'ui.router',
  'app.layout',
  'app.components',
  'app.home',
  'app.profile',
  'app.article',
  'app.services',
  'app.auth',
  'app.settings',
  'app.editor',
];

angular.module('app', requires);
angular.module('app')
  .constant('AppConstants', constants);
angular.module('app')
  .config(appConfig);
angular.module('app')
  .run(appRun);

angular.bootstrap(document, ['app'], {
  strictDi: true,
});
