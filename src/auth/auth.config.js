import authTemplate from './auth.html';

function AuthConfig($stateProvider) {
  'ngInject';

  $stateProvider

    .state('app.login', {
      url: '/login',
      controller: 'AuthCtrl as $ctrl',
      template: authTemplate,
      title: 'Sign in',
      resolve: {
        auth: function(User) {
          return User.ensureAuthIs(false);
        },
      },
    })

    .state('app.register', {
      url: '/register',
      controller: 'AuthCtrl as $ctrl',
      template: authTemplate,
      title: 'Sign up',
      resolve: {
        auth: function(User) {
          return User.ensureAuthIs(false);
        },
      },
    });

}

export default AuthConfig;
