import { IArticle, IAuthor, IComment } from '../store/article/types';

export interface IArticleService {
  query(config: object): Promise<object>;
  get(slug: string): Promise<IArticle>;
  destroy(slug: string): Promise<void>;
  save(article: IArticle): Promise<IArticle>;
  favorite(slug: string): Promise<void>;
  unfavorite(slug: string): Promise<void>;
}

export interface IUserService {
  current: IAuthor;
}

export interface ICommentService {
  add(slug: string, payload: string): Promise<IComment>;
  getAll(slug: string): Promise<IComment[]>;
  destroy(commentId: string, articleSlug: string): Promise<void>;
}

export interface IProfileService {
  follow(username: string): Promise<void>;
  unfollow(username: string): Promise<void>;
}
