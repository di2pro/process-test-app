import listErrorsTemplate from './list-errors.html';

const ListErrors = {
  bindings: {
    errors: '=',
  },
  template: listErrorsTemplate,
};

export default ListErrors;
