import articleMetaTemplate from './article-meta.html';

const ArticleMeta= {
  bindings: {
    article: '=',
  },
  transclude: true,
  template: articleMetaTemplate,
};

export default ArticleMeta;
