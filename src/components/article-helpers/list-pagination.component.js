import listTemplate from '../../components/article-helpers/list-pagination.html';

class ListPaginationCtrl {
  constructor($scope) {
    'ngInject';

    this._$scope = $scope;

  }

  pageRange(total) {
    const pages = [];

    for (var i = 0; i < total; i++) {
      pages.push(i + 1);
    }

    return pages;
  }

  changePage(number) {
    this._$scope.$emit('setPageTo', number);
  }


}

const ListPagination= {
  bindings: {
    totalPages: '=',
    currentPage: '=',
  },
  controller: ListPaginationCtrl,
  template: listTemplate,
};

export default ListPagination;
