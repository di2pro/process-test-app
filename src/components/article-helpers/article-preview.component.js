import articlePreview from './article-preview.html';

const ArticlePreview = {
  bindings: {
    article: '=',
  },
  template: articlePreview,
};

export default ArticlePreview;
