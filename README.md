## Process.st - Test App

I've updated some parts of the code because of the old versions, apart from that the main changes related to introduction react with redux for state management. Also I set up all the configs for linting, testing and running the project.

```bash
npm i && npm start
```
to start the project and
```bash
npm test
```
for testing

If you'd like to keep jest watch the files use
```bash
npm test -- --watch
```

What's missing, yes, unfortunately, there some parts that I haven't accomplished yet:

- **Comment Form.** Need to be added a form with validation and handlers to add new comments. I'd use formik with yup for it since I don't need to store the message in the redux store, after creating I'll send an action.
- **Comments.** At the moment there is no handler for removing the comment
- **Rating.** At the moment user can vote multiple times, mostly it shows how the rating is being calculated.
- **Unit tests.** The coverage at the moment isn't perfect and also the coverage set up is missing
- **Errors.** I haven't worked on this part, so, error cases for some async logic are missing and that's definitely must be done as soon as possible
- **CI/CD.** There are no pipelines at the moment, but I'll add them and deploy the application to GitLab Pages.

