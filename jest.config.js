module.exports = {
  'roots': [
    '<rootDir>/src',
  ],
  'transform': {
    '^.+\\.tsx?$': 'ts-jest',
  },
  'setupFilesAfterEnv': ['jest-enzyme'],
  'testEnvironment': 'enzyme',
  'testEnvironmentOptions': {
    'enzymeAdapter': 'react16',
  },
};
